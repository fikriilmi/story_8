from django.test import TestCase
import unittest
import time
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import all_in
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class Lab8UnitTest(TestCase):
 	
 	#ini untuk cek views apakah ada all_in 
	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, all_in)
 
 	#ini untuk url ga masuk
	def test_url_is_not_exist(self):
		response = Client().get("/wawa")
		self.assertEqual(response.status_code, 404)

